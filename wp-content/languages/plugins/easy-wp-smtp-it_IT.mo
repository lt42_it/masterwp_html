��    .      �  =   �      �  ,   �          /     J     W      m  #   �  2   �     �  	   �          
          !  ~   &  7   �  3   �               0     D  	   `     j  	   x     �     �     �     �     �     �     �     �     �     �       )        =  )   Z  4   �  +   �  *   �               &     *  !  ;     ]	     c	     y	     �	     �	  %   �	  ,   �	  @   
     Q
     j
  	   x
     �
     �
     �
  �   �
  @   *  @   k     �     �     �  #   �  	   	       
   !     ,     >     O     W     `     p     �     �     �     �     �  +   �     	  .   %  9   T  5   �  .   �     �     �     
             %              !                      #                )                         	                 +                     *          .       &   (              $   "                      -   ,   '      
        "Note" as in "Note: keep this in mind"Note: Block all emails Don't Replace "From" Field Easy WP SMTP Easy WP SMTP Settings Enter a subject for your message Enter the recipient's email address For most servers SSL/TLS is the recommended option From Email Address From Name Message No Nonce check failed. None Please configure your SMTP credentials in the <a href="%s">settings menu</a> in order to send email using Easy WP SMTP plugin. Please enter a valid email address in the 'FROM' field. Please enter a valid port in the 'SMTP Port' field. Rate Us Reply-To Email Address SMTP Authentication SMTP Configuration Settings SMTP Host SMTP Password SMTP Port SMTP Settings SMTP Username SSL/TLS STARTTLS Save Changes Send Test Email Settings Settings are not saved. Settings saved. Subject Support Forum The password to login to your mail server The port to your mail server The username to login to your mail server This email address will be used in the 'From' field. This options should always be checked 'Yes' This text will be used in the 'FROM' field To Type of Encryption Yes Your mail server PO-Revision-Date: 2019-01-08 16:27:19+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: it
Project-Id-Version: Plugins - Easy WP SMTP - Development (trunk)
 Nota: Blocca tutte le email Non sostituire il campo "Da" Easy WP SMTP Impostazioni Easy WP SMTP Inserisci l'oggetto del tuo messaggio Inserisci l'indirizzo email del destinatario Per la maggior parte dei server SSL/TLS è l'opzione consigliata Indirizzo email mittente Nome mittente Messaggio No Nonce non riuscito. Nessuno Si prega di configurare le credenziali SMTP nel <a href="%s"> menu Impostazione </a> al fine di inviare e-mail utilizzando Easy WP SMTP. Si prega di inserire un indirizzo email valido nel campo ' DA' . Si prega di inserire una porta valida nel campo ' Porta SMTP ' . Valutaci Indirizzo email di risposta Autenticazione SMTP Impostazioni di configurazione SMTP Host SMTP Password SMTP Porta SMTP Impostazioni SMTP Nome Utente SMTP SSL/TLS STARTTLS Salva modifiche Invia una mail di Test Impostazioni Impostazioni non salvate Impostazioni salvate. Oggetto Forum di supporto La password per il login al server di posta La porta al server di posta Il nome utente per il login al server di posta Questo indirizzo e-mail verrà utilizzato nel campo 'Da'  Questa opzione deve essere sempre impostata su ' Sì' Questo testo verrà utilizzato nel campo ' DA' A Tipo di Crittografia Sì Il tuo server di posta 